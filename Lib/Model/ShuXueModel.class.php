<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShuXueModel
 *
 * @author wooddoor-ljf
 */
class ShuXueModel extends RelationModel {

    protected $_link = array(
        'XueSheng' => array(
            'mapping_type' => BELONGS_TO,
            'class_name' => 'XueSheng',
            'foreign_key' => 'XueSheng_id',
            'as_fields' => 'BanBie,XueHao,ZuoWeiHao,ZuoWeiHao_prev,XingMing',
        ),
    );
    protected $_map = array(
        '学生id' => 'XueSheng_id',
        '数学id' => 'id',
        '班别' => 'BanBie',
        '学号' => 'XueHao',
        '座位号' => 'ZuoWeiHao',
        '上次座位号' => 'ZuoWeiHao_prev',
        '姓名' => 'XingMing',
        '总分' => 'ZongFen',
    );

}
