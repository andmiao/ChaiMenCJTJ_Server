<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShuXueViewModel
 *
 * @author wooddoor-ljf
 */
class ShuXueViewModel extends XKMapViewModel {

    /*public function __construct($xk) {
        // 无效，因为在Model的构造函数中已经要创建数据库连接了，在这个构造函数中修改viewFields已经太迟了，只能每个学科建一个ViewModel了
        $this->viewFields[$xk] = $this->xkDB;
        $this->xsDB['_on'] = $xk . '.XueSheng_id=XueSheng.id';
        $tmp['XueSheng'] = $this->xsDB;
        $this->viewFields['XueSheng'] = $this->xsDB;
    }*/

    public $viewFields = array(
              'ShuXue' => array('id'=>'ShuXue_id','XueSheng_id','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','ZongFen','XiuGaiShiJian'),
              'XueSheng' => array('KaoShi_id','BanBie','XueHao','ZuoWeiHao','ZuoWeiHao_prev','XingMing', '_on'=>'ShuXue.XueSheng_id=XueSheng.id'),
    );

}
