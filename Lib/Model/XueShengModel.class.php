<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XueSheng
 *
 * @author wooddoor-ljf
 */
class XueShengModel extends RelationModel {

    protected $_map = array(
        '班别' => 'BanBie',
        '学号' => 'XueHao',
        '座位号' => 'ZuoWeiHao',
        '上次座位号' => 'ZuoWeiHao_prev',
        '姓名' => 'XingMing',
        '语文' => 'YuWen',
        '数学' => 'ShuXue',
        '英语' => 'YingYu',
        '物理' => 'WuLi',
        '化学' => 'HuaXue',
        '生物' => 'ShengWu',
        '政治' => 'ZhengZhi',
        '历史' => 'LiShi',
        '地理' => 'DiLi',
        '总分' => 'ZongFen',
        '身份证' => 'ShenFenZheng',
        '其它' => 'QiTa',
        '总分' => 'ZongFen',
        '修改时间' => 'XiuGaiShiJian',
    );
    protected $_link = array();
    protected $link_all = array(
        'YuWen' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'YuWen',
            'mapping_name' => 'YuWen',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:语文',
        ),
        'ShuXue' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'ShuXue',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:数学',
        ),
        'YingYu' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'YingYu',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:英语',
        ),
        'WuLi' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'WuLi',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:物理',
        ),
        'HuaXue' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'HuaXue',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:化学',
        ),
        'ShengWu' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'ShengWu',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:生物',
        ),
        'ZhengZhi' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'ZhengZhi',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:政治',
        ),
        'LiShi' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'LiShi',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:历史',
        ),
        'DiLi' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'DiLi',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:地理',
        ),/*
        'YuWenView' => array(
            'mapping_type' => HAS_ONE,
            'class_name' => 'YuWenView',
            'foreign_key' => 'XueSheng_id',
            //'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => 'ZongFen:YuWenView',
        ),*/
    );
    protected $link_single = array(
            'mapping_type' => HAS_ONE,
            'class_name' => '',
            'foreign_key' => 'XueSheng_id',
            'mapping_fields' => 'ZongFen,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,XiuGaiShiJian',
            'as_fields' => "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,ZongFen:总分,XiuGaiShiJian:修改时间",
    );
    
    /**
     * 运行时设置$_link实现根据实际需求使用不同的关联模型
     * 返回类对象以实现连贯操作
     * @param type $xk：为空表示取所有科目成绩，传入具体科目名(YuWen、ShuXue、YingYu、WuLi、HuaXue、ShengWu、ZhengZhi、LiShi、DiLi)则获取该科目的详细成绩
     * $xkSheZhi：各学科每个小题的别名，应该从数据库中读取到
     */
    public function setLink($xk, $xkSheZhi='') {
        if($xk) {
            if(!empty($xkSheZhi)) {
                $xkSheZhi = str_replace('，', ',', $xkSheZhi);
                $mapping_fields = str_replace('=', ' as ', $xkSheZhi) . ',ZongFen,XiuGaiShiJian';
                $as_fields_arr = explode(',', $xkSheZhi);
                $as_fields = '';
                foreach ($as_fields_arr as $value) {
                    $field = explode('=', $value);
                    $as_fields = $as_fields . $field[1] . ',';
                }
                $as_fields = $as_fields . 'ZongFen:总分,XiuGaiShiJian:修改时间';
                $this->link_single['mapping_fields'] = $mapping_fields;
                $this->link_single['as_fields'] = $as_fields;
            }
            $this->link_single['class_name'] = $xk;
            $this->_link[$xk] = $this->link_single;
        }
        else {
            $this->_link = $this->link_all;
        }
        return $this;
    }

    /**
     * 将这个函数复制到Model.class.php替换掉原ThinkPHP的parseFieldsMap函数
     * 处理字段映射
     * access public
     * @param array $data 当前数据
     * @param integer $type 类型 0 写入 1 读取
     * return array
     */
    public function parseFieldsMap($data, $type = 1) {
        // 检查字段映射
        if (!empty($this->_map)) {
            foreach ($data as $dkey => $dval) {
                if (is_array($dval)) {
                    $data[$dkey] = self::parseFieldsMap($dval, $type);
                } else {
                    foreach ($this->_map as $key => $val) {
                        if ($type == 1) { // 读取
                            if ($dkey != $val) {
                                continue;
                            }
                            //if (isset($data[$val])) {//因为是从数据库读取后要转换字段名，而从数据库读取到的数据有些是null的，这些字段名显然也必须转换，所以这个isset不要
                            $data[$key] = $data[$val];
                            unset($data[$val]);
                            //}
                        } else {
                            if ($dkey != $key) {
                                continue;
                            }
                            if (isset($data[$key])) {
                                $data[$val] = $data[$key];
                                unset($data[$key]);
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }

}
