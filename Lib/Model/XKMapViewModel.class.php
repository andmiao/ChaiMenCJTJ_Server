<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of map
 * 每个学科的map都是一样的，这里设好后，各学科继承这个类就无需每个学科都设置一次map了
 * @author wooddoor-ljf
 */
class XKMapViewModel extends ViewModel {
    protected $_map = array(
        '学生id' => 'XueSheng_id',
        '语文id' => 'YuWen_id',
        '数学id' => 'ShuXue_id',
        '英语id' => 'YingYu_id',
        '物理id' => 'WuLi_id',
        '化学id' => 'HuaXue_id',
        '生物id' => 'ShengWu_id',
        '政治id' => 'ZhengZhi_id',
        '历史id' => 'LiShi_id',
        '地理id' => 'DiLi_id',
        '班别' => 'BanBie',
        '学号' => 'XueHao',
        '座位号' => 'ZuoWeiHao',
        '上次座位号' => 'ZuoWeiHao_prev',
        '姓名' => 'XingMing',
        '总分' => 'ZongFen',
    );
}
