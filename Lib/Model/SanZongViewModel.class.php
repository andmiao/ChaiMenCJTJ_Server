<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SanZongViewModel
 *
 * @author wooddoor-ljf
 */
class SanZongViewModel extends ViewModel {

    public $viewFields = array(
              'XueSheng' => array('KaoShi_id','BanBie','XueHao','ZuoWeiHao','ZuoWeiHao_prev','XingMing'),
              'YuWen' => array('ZongFen'=>'语文', '_on'=>'YuWen.XueSheng_id=XueSheng.id'),
              'ShuXue' => array('ZongFen'=>'数学', '_on'=>'ShuXue.XueSheng_id=XueSheng.id'),
              'YingYu' => array('ZongFen'=>'英语', '_on'=>'YingYu.XueSheng_id=XueSheng.id'),
    );
}
