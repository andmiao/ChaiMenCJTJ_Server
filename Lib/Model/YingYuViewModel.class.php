<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of YingYuViewModel
 *
 * @author wooddoor-ljf
 */
class YingYuViewModel extends XKMapViewModel {

    public $viewFields = array(
              'YingYu' => array('id'=>'YingYu_id','XueSheng_id','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','ZongFen','XiuGaiShiJian'),
              'XueSheng' => array('KaoShi_id','BanBie','XueHao','ZuoWeiHao','ZuoWeiHao_prev','XingMing', '_on'=>'YingYu.XueSheng_id=XueSheng.id'),
    );
}
